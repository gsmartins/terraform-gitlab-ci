provider "aws" {
  region = "us-east-1"
  profile = "ec2"
}
variable "instance_count" {
  default = "1"
}

resource "aws_instance" "ec2teste" {
  ami = "ami-064a0193585662d74"
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.ec2teste.name}"]
}

resource "aws_security_group" "ec2teste" {
  name = "habilita_acesso_externo"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

